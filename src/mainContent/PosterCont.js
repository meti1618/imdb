import React from 'react'
import './headerCont.css'
const PosterCont = (props) => {
    return(
        <div className="PosterCont">
            <img className={props.class} src={props.src1} />
            <img id="marginingPoster" className={props.class} src={props.src2} />
            <img className={props.class} src={props.src3} />
        </div>
    )
} 

export default PosterCont ;
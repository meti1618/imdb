import React from 'react'
import './headerCont.css'
import {FaFacebookF} from 'react-icons/fa'
import {FaLongArrowAltRight} from 'react-icons/fa'
import imog from './imog.png'
const BoxLinkedHeader = (props) => {
    return(
        <div className="BoxLinkedHeader">
           <a href="https://www.facebook.com" className="Facebook-Box">
               <FaFacebookF className="FaFacebookF"/>
               <div className="Facebook-Box--text">
                    <p>Follow Uz on Facebock</p>
                    <FaLongArrowAltRight className="FaLongArrowAltRight"/>
               </div>
           </a>
           <a href="https://www.imdb.com/title/tt8971486/" className="SnapShot-Box">
               <img src={imog} className="SnapShot-Box--imog"/>
               <div className="SnapShot-Box--text">
                    <p>IMDb SnapShot</p>
                    <span>2019 Emmys</span>
               </div>
           </a>
        </div>
    )
} 

export default BoxLinkedHeader ;
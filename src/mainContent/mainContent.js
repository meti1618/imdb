import { Component } from 'react';
import HeaderCont from './headerCont'
import PosterCont from './PosterCont'
import Bluecae from './Bluecae.jpg'
import trancfor from './trancfor.jpg'
import abominable from './abominable.jpg'
import './mainContent.css'
class MainContent extends Component{

  render() {
    return(
      <div className="MainContent">
              <h2 className="MainContent--TitleTheme">Explore</h2>
              <HeaderCont/>
              <h2 className="MainContent--TitleTheme2"><span>Poll: </span>Top Movies 2019</h2>
              <PosterCont class="POSTERIMG" src1={Bluecae} src2={trancfor} src3={abominable} />
      </div>

    );
  }
}





export default MainContent;
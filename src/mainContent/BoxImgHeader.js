import React from 'react'
import './headerCont.css'
import {FaPlay} from 'react-icons/fa'
const BoxImgHeader = (props) => {
    return(
        <a href={props.link} id="BoxImgHeader" className={props.class} >
            <FaPlay className="FaPlay"/>
            <p>{props.subImg}</p>
        </a>
    )
} 

export default BoxImgHeader ;
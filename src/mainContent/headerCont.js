import React from 'react'
import './headerCont.css'
import BoxImgHeader from './BoxImgHeader'
import BoxLinkedHeader from './BoxLinkedHeader'
import IMDB from './IMDB.png'
const HeaderCont = (props) => {
    return(
        <div className="headerCont">
            <h2>{props.hi}</h2>
            <div className="searchBox">
                <input className="search--input" type="search" placeholder="Find Movies, TV shows, Celebrity and more ..."/>
                <span className="searchBox--number">5</span>
                <a href="https://www.imdb.com/search/" className="searchBox--text">All Finds</a>
            </div>
            <div className="BoxHeader">
                <BoxImgHeader link="https://www.imdb.com/title/tt6105098/" class="Lionking" subImg="The Lion King (2019)"/>
                <BoxLinkedHeader/>
                <BoxImgHeader link="https://www.imdb.com/title/tt1206885/" class="Rambo" />
                <div className="endBox">
                    <a href="https://www.imdb.com/search/" className="viewAllBox">VIEW ALL</a>
                    <a href="https://www.imdb.com" className="IMDBBox"><img className="IMDBImg" src={IMDB}/></a>
                </div>
            </div>

        </div>
    )
} 

export default HeaderCont ;

import React from 'react';

import './Nav.css'
 


const DropDownNav = (props) => {

    return(    
        <ul className="DropDownNav" >
            <li >
            <a href={props.href1}>    {props.itemOne} <span>.</span> </a>
            </li>
            <li >
            <a href={props.href2}>       {props.itemTwo} <span>.</span> </a>
            </li>
            <li >
            <a>      {props.itemThree} <span>.</span> </a>
            </li>
        </ul>
    );
    
}
  

  export default DropDownNav  ;
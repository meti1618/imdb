import logo from './logo.png'
import './Nav.css';
import { Component } from 'react';
import itemNAV from './itemNav'
import DropDownNav from './dropdownnav'
import { FaAngleDown } from 'react-icons/fa';
import {FaBars} from 'react-icons/fa'
class Nav extends Component{

  state={
    bol:false,
    bol2:false,
    bol3:false,
    Huber:true
  };
   DopdownNavbar = () =>{
      const dosShow = this.state.bol;
      this.setState({bol : !dosShow})
  }
  DopdownNavbar2 = () =>{
  const dosShow2 = this.state.bol2;
  this.setState({bol2 : !dosShow2})
} 
DopdownNavbar3 = () =>{
  const dosShow3 = this.state.bol3;
  this.setState({bol3 : !dosShow3})
}
HamburNav = () => {
  const dosShow4 = this.state.Huber;
  this.setState({Huber : !dosShow4})
}

  render() {
    return(
      <nav>
          <img src={logo} className="nav-logo" alt="logo" />
          { this.state.Huber===true ? <ul className="nav-text">
            <itemNAV  className="nav--text--item">Movies {"&"} TV Shows <FaAngleDown className="FaAngleDown1" onClick={this.DopdownNavbar}  />{ this.state.bol===true ? <DropDownNav itemOne="Top movie" itemTwo="Best TV Shows" itemThree="Proposal IMDB" href1="https://www.imdb.com/list/ls043474895/" href2="https://www.imdb.com/list/ls043004346/" ></DropDownNav> :null}</itemNAV>
            <itemNAV className="nav--text--item">Evenis {"&"} Photos <FaAngleDown className="FaAngleDown2" onClick={this.DopdownNavbar2}/>{ this.state.bol2===true ? <DropDownNav itemOne="Top Evenis" itemTwo="Best Photos" itemThree="Photos IMDB" href1="https://www.imdb.com/best-of/" href2="https://www.imdb.com/gallery/rg784964352"></DropDownNav> :null}</itemNAV>
            <itemNAV className="nav--text--item">News {"&"} Community <FaAngleDown className="FaAngleDown3" onClick={this.DopdownNavbar3}/>{ this.state.bol3===true ? <DropDownNav itemOne="Top News" itemTwo="Best Community" itemThree="News IMDB" href1="https://www.imdb.com/news/top"></DropDownNav> :null}</itemNAV> 
          </ul>:null
          }
          
            <FaBars className="humber-nav" onClick={this.HamburNav}/>
        
      </nav>
    );
  }
}


export default Nav;

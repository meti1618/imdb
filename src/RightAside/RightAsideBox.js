import React from 'react'
import './RightAside.css'
import {FaTwitter} from 'react-icons/fa';
import {FaCheckCircle} from 'react-icons/fa';
import {FaEllipsisV} from 'react-icons/fa'
import {FaHeart} from 'react-icons/fa'
import imdb from './768px-Imdb_logo_rounded_corners.png'

const RightAsideBox = (props) => {



    return(
    
        <div className="RightAsideBox">
            
                <a href="https://twitter.com">
                    <FaTwitter className="FaTwitter"/>
                </a>
            
            <div className="RightAsideBox--sub-header">
            <a className="RABox--S-header--left" href="https://twitter.com">
                
                
                    <img className="BoxLogoImdb" src={imdb} width="26px"height="26px"/>
                    
                    <div>
                        <div>
                        
                         <p>IMDb</p>
                         <FaCheckCircle className="FaCheckCircle"/>
                        </div>
                         <span>@imdb</span>
                    </div>
                
                </a>
                <div className="RABox--S-header--right">
                    <FaEllipsisV className="FaEllipsisV" ></FaEllipsisV>
                    <span className="toltip "><p>fallow</p> <p>shyre</p> </span>
                </div>
            </div>
            <h3>{props.title}</h3>
            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi soluta officiis...</p>
            {props.children}
        </div> 
     
    )
}

export default RightAsideBox; 
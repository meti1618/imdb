import './LeftAside.css';
import React from 'react';
import LeftAsideInnerItem from './LeftAsideInnerItem'
import lionKing from './lionKing.png'
import Itc2Logo from './itc2Log.jpg'
import rambo from './rambo.jpg'
import lizzo from './lizzo.jpg'
import Downton from './Downton.jpg'
import icon from './plus.svg'
import icon2 from './minus.svg'
const LeftAsideItem = (props) =>{

    return(
    <div>      
        <div id="LeftAsideItem1" className="LeftAsideItem">
            <span  className="TitleContent" id="TitleContent" >{props.TitleContent }   <img onClick={ChengerIcon}  className="iconsToggler" id="iconsToggler" src={icon}/> </span>
            <div className="TogglerInnerItem" id="TogglerInnerItem">
                <LeftAsideInnerItem Title="The  Lion  King" Sales="$ 337 M" Score="6.9" href="https://www.imdb.com/title/tt6105098/"><img className="logoMovie" src={lionKing}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="It Chapter Two" Sales="$ 170 M" Score="6.5" href="https://www.imdb.com/title/tt7349950/"><img className="logoMovie" src={Itc2Logo}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="Rambo: Last Blood" Sales="$ 47 M" Score="6.1" href="https://www.imdb.com/title/tt1206885/"><img className="logoMovie" src={rambo}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="Hunters" Sales="$ 16 M" Score="7.2" href="https://www.imdb.com/title/tt7456722/"><img className="logoMovie" src={lizzo}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="Downton Abbey" Sales="$ 31 M" Score="7.4" href="https://www.imdb.com/title/tt6398184/"><img className="logoMovie" src={Downton}/></LeftAsideInnerItem>
                <a className="foter-LeftAsideInnerItem">MORE MOVIES</a>
            </div>
            
        </div>
        <div id="LeftAsideItem2" className="LeftAsideItem">
            <span  className="TitleContent" id="TitleContent2"  >{props.TitleContent2}  <img onClick={ChengerIcon2} className="iconsToggler" id="iconsToggler2" src={icon}/> </span>
            <div className="TogglerInnerItem" id="TogglerInnerItem2">
                <LeftAsideInnerItem Title="The  Lion  King" Sales="$ 337 M" Score="6.9" href="https://www.imdb.com/title/tt6105098/"><img className="logoMovie" src={lionKing}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="It Chapter Two" Sales="$ 170 M" Score="6.5" href="https://www.imdb.com/title/tt7349950/"><img className="logoMovie" src={Itc2Logo}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="Rambo: Last Blood" Sales="$ 47 M" Score="6.1" href="https://www.imdb.com/title/tt1206885/"><img className="logoMovie" src={rambo}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="Hunters" Sales="$ 16 M" Score="7.2" href="https://www.imdb.com/title/tt7456722/"><img className="logoMovie" src={lizzo}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="Downton Abbey" Sales="$ 31 M" Score="7.4" href="https://www.imdb.com/title/tt6398184/"><img className="logoMovie" src={Downton}/></LeftAsideInnerItem>
                <a className="foter-LeftAsideInnerItem">MORE MOVIES</a>
            </div>
        </div>
        <div id="LeftAsideItem3" className="LeftAsideItem">
            <span  className="TitleContent" id="TitleContent3" >{props.TitleContent3}  <img  onClick={ChengerIcon3} className="iconsToggler" id="iconsToggler3" src={icon}/> </span>
            <div className="TogglerInnerItem" id="TogglerInnerItem3">
                <LeftAsideInnerItem Title="The  Lion  King" Sales="$ 337 M" Score="6.9" href="https://www.imdb.com/title/tt6105098/"><img className="logoMovie" src={lionKing}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="It Chapter Two" Sales="$ 170 M" Score="6.5" href="https://www.imdb.com/title/tt7349950/"><img className="logoMovie" src={Itc2Logo}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="Rambo: Last Blood" Sales="$ 47 M" Score="6.1" href="https://www.imdb.com/title/tt1206885/"><img className="logoMovie" src={rambo}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="Hunters" Sales="$ 16 M" Score="7.2" href="https://www.imdb.com/title/tt7456722/"><img className="logoMovie" src={lizzo}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="Downton Abbey" Sales="$ 31 M" Score="7.4" href="https://www.imdb.com/title/tt6398184/"><img className="logoMovie" src={Downton}/></LeftAsideInnerItem>
                <a className="foter-LeftAsideInnerItem">MORE MOVIES</a>
            </div>
        </div>
        <div id="LeftAsideItem4" className="LeftAsideItem">
            <span  className="TitleContent" id="TitleContent4"  >{props.TitleContent4}  <img  onClick={ChengerIcon4} className="iconsToggler" id="iconsToggler4" src={icon}/> </span>
            <div className="TogglerInnerItem" id="TogglerInnerItem4">
                 <LeftAsideInnerItem Title="The  Lion  King" Sales="$ 337 M" Score="6.9" href="https://www.imdb.com/title/tt6105098/"><img className="logoMovie" src={lionKing}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="It Chapter Two" Sales="$ 170 M" Score="6.5" href="https://www.imdb.com/title/tt7349950/"><img className="logoMovie" src={Itc2Logo}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="Rambo: Last Blood" Sales="$ 47 M" Score="6.1" href="https://www.imdb.com/title/tt1206885/"><img className="logoMovie" src={rambo}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="Hunters" Sales="$ 16 M" Score="7.2" href="https://www.imdb.com/title/tt7456722/"><img className="logoMovie" src={lizzo}/></LeftAsideInnerItem>
                <LeftAsideInnerItem Title="Downton Abbey" Sales="$ 31 M" Score="7.4" href="https://www.imdb.com/title/tt6398184/"><img className="logoMovie" src={Downton}/></LeftAsideInnerItem>
                <a className="foter-LeftAsideInnerItem">MORE MOVIES</a>
            </div>
        </div>

    </div>  
    )


}
let Bol = true ; 
function ChengerIcon () {
    if (Bol===true) {
    document.getElementById("iconsToggler").src = icon2;
    document.getElementById("iconsToggler").style.filter=" invert(100%) sepia(100%) saturate(0%) hue-rotate(25deg) brightness(104%) contrast(103%)";
    document.getElementById("LeftAsideItem1").style.backgroundColor = "#252525";
    document.getElementById("TitleContent").style.color="#e0e0e0"
    document.getElementById("TogglerInnerItem").style.display="block"
    Bol = false;
    }else{
    document.getElementById("iconsToggler").src = icon; 
    document.getElementById("iconsToggler").style.filter="invert(52%) sepia(1%) saturate(2565%) hue-rotate(316deg) brightness(69%) contrast(56%) ";
    document.getElementById("LeftAsideItem1").style.backgroundColor = "#141414";
    document.getElementById("TitleContent").style.color="#bababa"
    document.getElementById("TogglerInnerItem").style.display="none"
    Bol = true ;   
    }
}




let Bol2 = true ; 
function ChengerIcon2 () {
    if (Bol2===true) {
    document.getElementById("iconsToggler2").src = icon2;
    document.getElementById("iconsToggler2").style.filter=" invert(100%) sepia(100%) saturate(0%) hue-rotate(25deg) brightness(104%) contrast(103%)";
    document.getElementById("LeftAsideItem2").style.backgroundColor = "#252525";
document.getElementById("TitleContent2").style.color="#e0e0e0"
document.getElementById("TogglerInnerItem2").style.display="block"
    Bol2 = false;
    }else{
    document.getElementById("iconsToggler2").src = icon; 
    document.getElementById("iconsToggler2").style.filter="invert(52%) sepia(1%) saturate(2565%) hue-rotate(316deg) brightness(69%) contrast(56%) ";
    document.getElementById("LeftAsideItem2").style.backgroundColor = "#141414";
document.getElementById("TitleContent2").style.color="#bababa"
document.getElementById("TogglerInnerItem2").style.display="none"
Bol2 = true ;   
    }
}

let Bol3 = true ; 
function ChengerIcon3 () {
    if (Bol3===true) {
    document.getElementById("iconsToggler3").src = icon2;
    document.getElementById("iconsToggler3").style.filter=" invert(100%) sepia(100%) saturate(0%) hue-rotate(25deg) brightness(104%) contrast(103%)";
        document.getElementById("LeftAsideItem3").style.backgroundColor = "#252525";
document.getElementById("TitleContent3").style.color="#e0e0e0"
document.getElementById("TogglerInnerItem3").style.display="block"
    Bol3 = false;
    }else{
    document.getElementById("iconsToggler3").src = icon; 
    document.getElementById("iconsToggler3").style.filter="invert(52%) sepia(1%) saturate(2565%) hue-rotate(316deg) brightness(69%) contrast(56%) ";
    document.getElementById("LeftAsideItem3").style.backgroundColor = "#141414";
document.getElementById("TitleContent3").style.color="#bababa"
document.getElementById("TogglerInnerItem3").style.display="none"
Bol3 = true ;   

    }
     
}

let Bol4 = true ; 
function ChengerIcon4 () {
    if (Bol4===true) {
    document.getElementById("iconsToggler4").src = icon2;
    document.getElementById("iconsToggler4").style.filter=" invert(100%) sepia(100%) saturate(0%) hue-rotate(25deg) brightness(104%) contrast(103%)";
        document.getElementById("LeftAsideItem4").style.backgroundColor = "#252525";
        document.getElementById("TitleContent4").style.color="#e0e0e0"
        document.getElementById("TogglerInnerItem4").style.display="block"
    Bol4 = false;
    }else{
    document.getElementById("iconsToggler4").src = icon; 
    document.getElementById("iconsToggler4").style.filter="invert(52%) sepia(1%) saturate(2565%) hue-rotate(316deg) brightness(69%) contrast(56%) ";
    document.getElementById("LeftAsideItem4").style.backgroundColor = "#141414";
document.getElementById("TitleContent4").style.color="#bababa"
document.getElementById("TogglerInnerItem4").style.display="none"
    Bol4 = true ;   
    }
}


export default LeftAsideItem;
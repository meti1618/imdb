import './LeftAside.css';
import { Component } from 'react';
import headerImg from './itc2.jpg'
import LeftAsideItem from './LeftAsideItem'
import {FaMinus} from 'react-icons/fa'
class LeftAside extends Component{




  render() {
    return(
        <aside className="LeftAside">
          <div className="header-box">
            <img className="headerImg" src={headerImg}/>
          </div>
            <LeftAsideItem  TitleContent="OPENING THIS WEEK" TitleContent4="AROUND THE WEB" TitleContent2="NOW PLAYING" TitleContent3="COMING SOON" />
        </aside>
    );
  }
}





export default LeftAside;
import React from 'react';
import './LeftAside.css';

const LeftAsideInnerItem = (props) =>{
    return (
        <div className="LeftAsideInnerItem">
            <a href={props.href} className="LeftAsideInnerItem--MainContent">
                    {props.children}
                <div className="text-innerItem">
                    <p className="Title-Movie">{props.Title}</p>
                    <span className="Sales-Movie">{props.Sales}</span>
                </div>
            </a>
                    <p className="Score-Movie">{props.Score}</p>
        </div>
    )
}



export default LeftAsideInnerItem